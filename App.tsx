import React from 'react';
import { activateKeepAwake } from 'expo-keep-awake';
import HomeScreen from './contexts/Home';
import SettingsScreen from './contexts/SettingsScreen';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

const App: React.FC = () => {

  if (__DEV__) {
    activateKeepAwake();
  }

  const Tab = createBottomTabNavigator();

  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="Home" component={HomeScreen} />
        <Tab.Screen name="Settings" component={SettingsScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default App;
